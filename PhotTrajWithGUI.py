#!/usr/bin/env python
# -*- coding: utf-8 -*-
from matplotlib.ticker import NullFormatter  # useful for `logit` scale
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import PySimpleGUI as sg
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
import sys
import math
from scipy import optimize
from scipy import integrate
from sympy.solvers import solve
from sympy import Symbol
from sympy import re, im, I, E
from sympy import sqrt
from scipy.signal import argrelmin,argrelmax
from scipy.integrate import odeint
from scipy.integrate import solve_ivp

import PySimpleGUI as sg      

sg.theme('DarkAmber')

def potential(r):
    potvalue = l*l/(2*r*r) - M*l*l/(r*r*r)
    return potvalue

while True:

    layout = [[sg.Text('Give Initial Values for Plot')],
              [sg.Text('Mass', size=(25, 1)), sg.InputText()],
              [sg.Text('Effective Energy', size=(25, 1)), sg.InputText()],
              [sg.Text('Angular Momentum/Mass (M)', size=(25, 1)), sg.InputText()],
              [sg.Text('Initial Distance (M)', size=(25, 1)), sg.InputText()],
              [sg.Text('Initial Angle (π)', size=(25, 1)), sg.InputText()],
              [sg.Text('Timestep', size=(25, 1)), sg.InputText()],
              [sg.Text('Total Time', size=(25, 1)), sg.InputText()],
              [sg.Text('Direction of Motion (0,1)', size=(25, 1)), sg.InputText()],

              [sg.Submit(), sg.Cancel()]]

    window = sg.Window('Schwarzschild Trajectory Plotter', layout)    

    event, values = window.read()    
    window.close()



    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212,projection='polar')

    if event == 'Cancel':
        sys.exit(0)

    #M = 10
    #l = 4.2*M
    #E = -0.025

    M = float(values[0])
    E = float(values[1])
    l = float(values[2])*M
    r0 = float(values[3])*M
    phi0 = float(values[4])*np.pi
    timestep = float(values[5])
    total_time = float(values[6])
    flag = int(values[7])

    if M < 1:
    	sg.popup('Value for mass is wrong.')
    elif r0 <= 2.1*M: 
    	sg.popup('Initial distance is wrong.')
    elif timestep < 0 or timestep > 10:
    	sg.popup('Step size is wrong.')
    elif total_time < 0:
    	sg.popup('Total_angle cannot be negative.')
    elif flag != 1 and flag != 0:
    	sg.popup('The value for direction must be 0 (ingoing) or 1 (outgoing).')
    elif E - potential(r0) < 0:
        sg.popup('Energy, angular momentum and initial distance compination is wrong, change one of them.')
    else:
    	break
# ax1 = plt.subplot(211)
# ax2 = plt.subplot(212,projection='polar')
# M = 10
# E = 0.005
# l = 5.2
# r0 = 3*M
# phi0 = 0
# timestep = 1
# total_time = 10000
# flag = 1
e = np.sqrt(2*E)
b = l/e
r_crit = 3*M #photon ring
t0=0
#total_time = 10000
iterations = total_time/timestep
p0 = [r0,phi0]
photRing = 0

def minustrajectory(r,t):
    r = r[0]
    mvalue = -np.sqrt(e*e-(l*l)/(r*r)+(2*M*l*l)/(r*r*r))
    phivalue = l/(r*r)
    return [float(mvalue), float(phivalue)]

def plustrajectory(r,t):
    r = r[0]
    pvalue = np.sqrt(e*e-(l*l)/(r*r)+(2*M*l*l)/(r*r*r))
    phivalue = l/(r*r)
    return [float(pvalue), float(phivalue)]


def potential(r):
    potvalue = l*l/(2*r*r) - M*l*l/(r*r*r)
    return potvalue

def solutions():
    ax1.plot(r_crit/M,potential(r_crit),"go")
    ax1.axhline(E)
    d = Symbol("d")
    a = solve(l*l/(2*d*d) - M*l*l/(d*d*d) - E, d,cubics=True,rationals=False)
    potPlot = np.linspace(2.1,30,800)
    ax1.plot(potPlot,potential(potPlot*M),'b-')
    ax1.set_xlabel('r/M')
    ax1.set_ylabel('$V_{eff}$')
    c = [ re(i) for i in a]
    if E == potential(r_crit):
        print("One solution only")
        c = [c[0]]
        print(c == r_crit)
        ax1.plot( c/M, potential(c),'ro')
    elif E < potential(r_crit):
        print("2 solutions")
        c = c[1:]
        for i in c:
            ax1.plot( i/M, potential(i),'ro')
    elif E > potential(r_crit):
        print("No solutions")
        c = []
    return c




def ivp(y0,t0,timestep,N,flag,total_time):
    n = int(total_time/timestep + 200)
    r = np.zeros( (n,1))
    phi = np.zeros( (n,1))
    i = 0
    r[i] = y0[0]
    phi[i] = y0[1]
    t0 = t0
    if N == 2:
        while t0 < total_time:
            ## Region I
            if r[i] < solutionsV[0] and flag == 0:
                l = [r[i],0]
                sol = odeint(minustrajectory,l, np.linspace(t0,t0+timestep,2))
                t0 = t0 + timestep
                R = sol[:,0]
                F = sol[:,1]
                r[i+1] = R[-1]
                phi[i+1] = F[-1] + phi[i]           
                if r[i+1] < 2.0*M:
                    break
            elif r[i] < solutionsV[0] and flag == 1:
                l = [r[i],0]
                sol = odeint(plustrajectory,l, np.linspace(t0,t0+timestep,2))
                t0 = t0 + timestep
                R = sol[:,0]
                F = sol[:,1]
                r[i+1] = R[-1]
                phi[i+1] = F[-1] + phi[i]              
                if np.isnan(r[i+1]):
                    flag = 0
                    r[i+1] = solutionsV[0]
                    continue
                if r[i+1] < 2.0*M:
                    break
            ## Region III
            elif r[i] > solutionsV[1] and flag == 0:
                l = [r[i],0]
                sol = odeint(minustrajectory,l, np.linspace(t0,t0+timestep,2))
                t0 = t0 + timestep
                R = sol[:,0]
                F = sol[:,1]
                r[i+1] = R[-1]
                phi[i+1] = F[-1] + phi[i]              
                if np.isnan(r[i+1]):
                    flag = 1
                    r[i+1] = solutionsV[1]
                    continue
                #r[i+1] = sol.y[0][-1]
                if r[i+1] < 2.0*M:
                    break
            elif r[i] > solutionsV[1] and flag == 1:
                l = [r[i],0]
                sol = odeint(plustrajectory,l, np.linspace(t0,t0+timestep,2))
                t0 = t0 + timestep
                R = sol[:,0]
                F = sol[:,1]
                r[i+1] = R[-1]
                phi[i+1] = F[-1] + phi[i]              
                if r[i+1] > 40.0*M:
                    break
            i = i + 1
    elif N == 1:
        while t0 < total_time:
            if r[i] < solutionsV[0] and flag == 0:
                l = [r[i],0]
                sol = odeint(minustrajectory,l, np.linspace(t0,t0+timestep,2))
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] < solutionsV[0] and flag == 1:
                l = [r[i],0]
                sol = odeint(plustrajectory,l, np.linspace(t0,t0+timestep,2))
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[0]
                    continue
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] == r_crit:
                photRing = 1
                break
            i = i + 1
    elif N == 0:
        while t0 < total_time:
            if flag == 0:
                l = [r[i],0]
                sol = odeint(minustrajectory,l, np.linspace(t0,t0+timestep,2))
                t0 = t0 + timestep
                R = sol[:,0]
                F = sol[:,1]
                r[i+1] = R[-1]
                phi[i+1] = F[-1] + phi[i]  
                if r[i+1] < 2.0*M:
                    break        
            elif flag == 1:
                l = [r[i],0]
                sol = odeint(plustrajectory,l, np.linspace(t0,t0+timestep,2))
                t0 = t0 + timestep
                R = sol[:,0]
                F = sol[:,1]
                r[i+1] = R[-1]
                phi[i+1] = F[-1] + phi[i]  
                if r[i+1] > 40.0*M:
                    break
            i = i + 1  
    else:
        print("More solutions than expected, check input values.")
    
    return r, phi


solutionsV = solutions()
N = len(solutionsV)

if N == 2 and abs(solutionsV[0]-solutionsV[1]) < 1e-3:
    photRing == 1


#flag = 1
#phi0 = 0
#step = 0.001
#total_angle = 6*np.pi

#r0 = solutionsV[2]-0.1*M
#thetazero = np.zeros((200000,1),dtype=object)
#soltheta = solve_ivp(dphidt,(0,total_time),thetazero[0],min_step=) 
#theta = soltheta.y[0]


if photRing == 1:
    phi = np.linspace(0,2*np.pi,1000)
    r = np.zeros(1000)
    for item in r:
        r[item] = r_crit
else:    
    r, phi = ivp(p0,t0,timestep,N,flag,total_time)


r = np.trim_zeros(r)
phi = np.trim_zeros(phi)
#phi = np.append(phi,phi[-1])
#ax = plt.subplot(111, projection='polar')
#ax = plt.subplot(111)

ax2.plot(phi0,r0,'b')
ax2.fill(np.linspace(0,2.0*np.pi,100),2*M*np.ones(100),"black",fill=True) 
ax2.plot(phi,r,'r')
ax2.set_yticklabels([])
ax2.set_xticklabels([])
ax2.grid(False)
if N==0 and flag == 1:
    ax2.set_ylim(0*M,40*M)
elif N==0 and flag == 0:
    ax2.set_ylim(0*M,r[0])
elif r[0] < solutionsV[0]:
    ax2.set_ylim(0*M,int(solutionsV[0]))
elif r[0] > solutionsV[1]:
    ax2.set_ylim(0,40*M)   
ax2.set_theta_zero_location('N')





plt.gca().yaxis.set_minor_formatter(NullFormatter())
plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                   wspace=0.35)
# plt.subplots_adjust(top=1, bottom=0.09, left=0.10, right=1, hspace=0.3,
#                     wspace=0.4)
fig = plt.gcf()      # if using Pyplot then get the figure from the plot
figure_x, figure_y, figure_w, figure_h = fig.bbox.bounds

# ------------------------------- END OF YOUR MATPLOTLIB CODE -------------------------------

# ------------------------------- Beginning of Matplotlib helper code -----------------------


def draw_figure(canvas, figure, loc=(0, 0)):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=2)
    return figure_canvas_agg
# ------------------------------- Beginning of GUI CODE -------------------------------


# define the window layout
layout = [[sg.Text('Potential and Trajectory Plots', font='Any 18')],
          [sg.Canvas(size=(figure_w, figure_h), key='canvas')],
          [sg.OK(pad=((figure_w / 2, 0), 3), size=(4, 2))]]

# create the form and show it without the plot
window = sg.Window('Schwarzschild Trajectory Plotter Photon',
                   layout, finalize=True)
# add the plot to the window
fig_canvas_agg = draw_figure(window['canvas'].TKCanvas, fig)

event, values = window.read()

window.close() 
