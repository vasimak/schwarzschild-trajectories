#!/usr/bin/env python
# -*- coding: utf-8 -*-
from matplotlib.ticker import NullFormatter  # useful for `logit` scale
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import PySimpleGUI as sg
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
import sys
import math
from scipy import optimize
from scipy import integrate
from sympy.solvers import solve
from sympy import Symbol
from sympy import re, im, I, E
from sympy import sqrt
from scipy.signal import argrelmin,argrelmax
from scipy.integrate import odeint
from scipy.integrate import solve_ivp

import PySimpleGUI as sg      

sg.theme('DarkAmber')

def potential(r):
    potvalue = - M/r + l*l/(2*r*r) - M*l*l/(r*r*r)
    return potvalue

while True:

    layout = [[sg.Text('Give Initial Values for Plot')],
              [sg.Text('Mass', size=(25, 1)), sg.InputText()],
              [sg.Text('Effective Energy', size=(25, 1)), sg.InputText()],
              [sg.Text('Angular Momentum/Mass (M)', size=(25, 1)), sg.InputText()],
              [sg.Text('Initial Distance (M)', size=(25, 1)), sg.InputText()],
              [sg.Text('Initial Angle (π)', size=(25, 1)), sg.InputText()],
              [sg.Text('Step', size=(25, 1)), sg.InputText()],
              [sg.Text('Total Angle (π)', size=(25, 1)), sg.InputText()],
              [sg.Text('Direction of Motion (0,1)', size=(25, 1)), sg.InputText()],

              [sg.Submit(), sg.Cancel()]]

    window = sg.Window('Schwarzschild Trajectory Plotter', layout)    

    event, values = window.read()    
    window.close()

    if event == 'Cancel':
        sys.exit(0)

    #M = 10
    #l = 4.2*M
    #E = -0.025

    M = float(values[0])
    E = float(values[1])
    l = float(values[2])*M
    r0 = float(values[3])*M
    phi0 = float(values[4])*np.pi
    step = float(values[5])
    total_angle = float(values[6])*np.pi
    flag = int(values[7])

    text = str("E="+str(E)+str("   ")+"V="+str(potential(r0)))

    if M < 1:
        sg.popup('Value for mass is wrong')
    elif r0 <= 2.1*M: 
        sg.popup('Initial distance is wrong')
    elif step < 0 or step > 10:
        sg.popup('Step size is wrong')
    elif total_angle < 0:
        sg.popup('Total_angle cannot be negative')
    elif flag != 1 and flag != 0:
        sg.popup('The value for direction must be 0 (ingoing) or 1 (outgoing)')
    elif E - potential(r0) < 0:
        sg.popup('Energy, angular momentum and initial distance compination is wrong, change one of them.',text)        
    else:
        break

ax1 = plt.subplot(211)
ax2 = plt.subplot(212,projection='polar')
e = np.sqrt(2.0*E+1)
r_min = (l*l-l*np.sqrt(l*l-12*M*M))/(2*M*M)
r_max = (l*l+l*np.sqrt(l*l-12*M*M))/(2*M*M)
ISCO = 0
def minustrajectory(phi,r):
    mvalue = -((r*r)/l)  *np.sqrt(e*e-(1-2*M/r)*(1+(l*l)/(r*r)))
    return mvalue

def plustrajectory(phi,r):
    pvalue = ((r*r)/l ) *np.sqrt(e*e-(1-2*M/r)*(1+(l*l)/(r*r)))
    return pvalue
    


def solutions():
    ax1.axhline(E)
    ax1.plot(r_min,potential(r_min*M),"go")
    ax1.plot(r_max,potential(r_max*M),"go")
    r = Symbol("r")
    a = solve(- M/r + l*l/(2*r*r) - M*l*l/(r*r*r) - (e*e-1)/2, r,cubics=True,rationals=False)
    potPlot = np.linspace(2.1,50,800)
    ax1.plot(potPlot,potential(potPlot*M),'b-')
    ax1.plot( r0/M, potential(r0),'bo')
    b = [ re(i) for i in a]
    if E < potential(r_max*M):
        print("One solution only")
        b = [b[0]]
        ax1.plot( b/M, potential(b),'ro')
    elif E > potential(r_min*M) and E < potential(2000*M):
        print("One solution only")
        b = [b[0]]
        ax1.plot( b/M, potential(b),'ro')
    elif E > potential(r_min*M) and E > potential(2000*M):
        print("No solution")
        b = []
    elif E > potential(r_max*M) and E < potential(2000*M) and E < potential(r_min*M):
        print("3 solutions")
        b = b
        for i in b:
            ax1.plot( i/M , potential(i),'ro')
    elif E < potential(r_min*M) and E > potential(2000*M):
        print("2 solutions")
        b = b[1:]
        for i in b:
            ax1.plot( i/M, potential(i),'ro')
    
    ax1.set_xlabel('r/M')
    ax1.set_ylabel('$V_{eff}$')    

    return b



def ivp(r0,phi0,step,N,flag,total_angle):
    n = int(total_angle/step)+10
    r = np.zeros( (n,1),dtype=object )
    r[0] = r0
    phi = phi0
    i = 0
    if N == 3:
        while True:
            ## Region III
            if r[i] < solutionsV[2] and r[i] > solutionsV[1] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                if np.isnan(sol.y[0][-1]):
                    flag = 1
                    r[i+1] = solutionsV[1]
                    continue
                r[i+1] = sol.y[0][-1]

            elif r[i] < solutionsV[2] and r[i] > solutionsV[1] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi = phi + step
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[2]
                    continue
                r[i+1] = sol.y[0][-1]

            ## Region I
            elif r[i] < solutionsV[0] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] < solutionsV[0] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[0]
                    continue
                if sol.y[0][-1] < 2.0*M:
                    break
            i = i + 1
            if phi >= total_angle:
                break  
    elif N == 2:
        while True:
            ## Region I
            if r[i] < solutionsV[0] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] < solutionsV[0] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[0]
                    continue
                if sol.y[0][-1] < 2.0*M:
                    break
            ## Region III
            elif r[i] > solutionsV[1] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                if np.isnan(sol.y[0][-1]):
                    flag = 1
                    r[i+1] = solutionsV[1]
                    continue
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] > solutionsV[1] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                
                if sol.y[0][-1] > 40.0*M:
                    break
            i = i + 1
    elif N == 1:
        while True:
            if r[i] < solutionsV[0] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] < solutionsV[0] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[0]
                    continue
                if sol.y[0][-1] < 2.0*M:
                    break
            i = i + 1
    elif N == 0:
        while True:
            if flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break        
            elif flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] > 40.0*M:
                    break
            i = i + 1     
    else:
        print("More solutions than expected, check input values.")
    
    return r


solutionsV = solutions()
N = len(solutionsV)
#flag = 1
#phi0 = 0
#step = 0.001
#total_angle = 6*np.pi

#r0 = solutionsV[2]-0.1*M
if abs(solutionsV[0]-solutionsV[1]) < 1e-3 and r0-rmin<1e-3 or r_max-r0<1e-3:
    ISCO == 1
if ISCO == 1:
    phi = np.linspace(0,2*np.pi,1000)
    r = np.zeros(1000)
    for item in r:
        r[item] = r_min
else:
    r = ivp(r0,phi0,step,N,flag,total_angle)


r = np.trim_zeros(r)
#ax = plt.subplot(111, projection='polar')
#ax = plt.subplot(111)

ax2.plot(phi0,r0,'b')
ax2.fill(np.linspace(0,2.0*np.pi,100),2*M*np.ones(100),"black",fill=True) 
ax2.plot(np.linspace(0,total_angle,len(r)),r,'r')
ax2.set_yticklabels([])
ax2.set_xticklabels([])
ax2.grid(False)
if N==0 and flag == 1:
    ax2.set_ylim(0*M,40*M)
elif N==0 and flag == 0:
    ax2.set_ylim(0*M,r[0])
elif r[0] < solutionsV[0]:
    ax2.set_ylim(0*M,int(solutionsV[0]))
elif r[0] > solutionsV[1]:
    ax2.set_ylim(0,40*M)   
ax2.set_theta_zero_location('N')





plt.gca().yaxis.set_minor_formatter(NullFormatter())
plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                    wspace=0.35)
fig = plt.gcf()      # if using Pyplot then get the figure from the plot
figure_x, figure_y, figure_w, figure_h = fig.bbox.bounds

# ------------------------------- END OF YOUR MATPLOTLIB CODE -------------------------------

# ------------------------------- Beginning of Matplotlib helper code -----------------------


def draw_figure(canvas, figure, loc=(0, 0)):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg
# ------------------------------- Beginning of GUI CODE -------------------------------


# define the window layout
layout = [[sg.Text('Potential and Trajectory Plots', font='Any 18')],
          [sg.Canvas(size=(figure_w, figure_h), key='canvas')],
          [sg.OK(pad=((figure_w / 2, 0), 3), size=(4, 2))]]

# create the form and show it without the plot
window = sg.Window('Schwarzschild Trajectory Plotter Test-Particle',
                   layout, finalize=True)

# add the plot to the window
fig_canvas_agg = draw_figure(window['canvas'].TKCanvas, fig)

event, values = window.read()

window.close() 
