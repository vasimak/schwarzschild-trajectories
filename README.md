# Schwarzschild Trajectories and Potential Plotter

## Please mind that this program works with the geometrized unit system, which means G=c=1

## Requirements
You must have python3 installed on your system, and with it you must have the following packages: numpy,sympy,matplotlib,pysimplegui and scipy.\
You can install them either with pip3 (the recommended way) or if you use the anaconda environment, with conda, as shown in the example:
```bash
pip3 install numpy 
```
```bash
conda install numpy 
```
### Test-Particles
Recommended stepsize for test-particles 0.01-0.001.\
Direction of motion equal to 0 means ingoing whereas equalt to 1 is outgoing.\
Only forward time propagation, meaning step and total angle must be positive.

### Photons
Photons share figures 1 and 4 with test-particles.\
Recommended stepsize for photons ~1.\
Since it calculates the trajectory based on proper time, high values of total time are preferred (~10000).
Direction of motion equal to 0 means ingoing whereas equal to 1 is outgoing.\
Only forward time propagation, meaning timestep and total time must be positive.

## 5 different situations for a test mass, depending on its energy, angular momentum and the BH's mass.
The horizontal line is the test's mass energy.
![Infalling-Outgoing](0root.png)
*Fig. 1: Plunge or outgoing trajectory*
![Infalling but far of the Rs](1root.png)
*Fig. 2: Freefalling but far from Schwarzschild Radius trajectory*
![Infalling but near the Rs](1root(2).png)
*Fig. 3: Freefalling but near the Schwarzschild Radius trajectory*
![Infalling near Rs-Scattering](2roots.png)
*Fig. 4: Freefalling near the Schwarzschild Radius or scattering trajectory*
![Infalling-Perihelion Shift](3roots.png)
*Fig. 5: Freefalling near the Schwarzschild Radius or Perihelion shifting bound trajectory*
