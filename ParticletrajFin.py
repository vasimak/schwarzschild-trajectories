#!/usr/bin/python3

import matplotlib.pyplot as plt
import math
import numpy as np
from scipy import optimize
from scipy import integrate
from sympy.solvers import solve
from sympy import Symbol
from sympy import re, im, I, E
from sympy import sqrt
from scipy.signal import argrelmin,argrelmax
from scipy.integrate import odeint
from scipy.integrate import solve_ivp


M = 10
l = 4.2*M
E = -0.025
e = np.sqrt(2.0*E+1)
r_min = (l*l-l*np.sqrt(l*l-12*M*M))/(2*M*M)
r_max = (l*l+l*np.sqrt(l*l-12*M*M))/(2*M*M)

def minustrajectory(phi,r):
    mvalue = -((r*r)/l)  *np.sqrt(e*e-(1-2*M/r)*(1+(l*l)/(r*r)))
    return mvalue

def plustrajectory(phi,r):
    pvalue = ((r*r)/l ) *np.sqrt(e*e-(1-2*M/r)*(1+(l*l)/(r*r)))
    return pvalue
    
def potential(r):
    potvalue = - M/r + l*l/(2*r*r) - M*l*l/(r*r*r)
    return potvalue

def solutions():
    r = Symbol("r")
    a = solve(- M/r + l*l/(2*r*r) - M*l*l/(r*r*r) - (e*e-1)/2, r,cubics=True,rationals=False)
    b = [ re(i) for i in a]
    if E < potential(r_max*M):
        print("One solution only")
        b = [b[0]]
        #plt.plot( b/M, potential(l,M,b),'ro')
    elif E > potential(r_min*M) and E < potential(2000*M):
        print("One solution only")
        b = [b[0]]
        #plt.plot( b/M, potential(l,M,b),'ro')
    elif E > potential(r_min*M) and E > potential(2000*M):
        print("No solution")
        b = []
    elif E > potential(r_max*M) and E < potential(2000*M) and E < potential(r_min*M):
        print("3 solutions")
        b = b
        #for i in b:
            #plt.plot( i/M , potential(l,M,i),'ro')
    elif E < potential(r_min*M) and E > potential(2000*M):
        print("2 solutions")
        b = b[1:]
        #for i in b:
            #plt.plot( i/M, potential(l,M,i),'ro')
    return b


def ivp(r0,phi0,step,N,flag,total_angle):
    n = int(total_angle/step)+10
    r = np.zeros( (n,1),dtype=object )
    r[0] = r0
    phi = phi0
    i = 0
    if N == 3:
        while True:
            ## Region III
            if r[i] < solutionsV[2] and r[i] > solutionsV[1] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                if np.isnan(sol.y[0][-1]):
                    flag = 1
                    r[i+1] = solutionsV[1]
                    continue
                r[i+1] = sol.y[0][-1]

            elif r[i] < solutionsV[2] and r[i] > solutionsV[1] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi = phi + step
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[2]
                    continue
                r[i+1] = sol.y[0][-1]

            ## Region I
            elif r[i] < solutionsV[0] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] < solutionsV[0] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[0]
                    continue
                if sol.y[0][-1] < 2.0*M:
                    break
            i = i + 1
            if phi >= total_angle:
                break  
    elif N == 2:
        while True:
            ## Region I
            if r[i] < solutionsV[0] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] < solutionsV[0] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[0]
                    continue
                if sol.y[0][-1] < 2.0*M:
                    break
            ## Region III
            elif r[i] > solutionsV[1] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                if np.isnan(sol.y[0][-1]):
                    flag = 1
                    r[i+1] = solutionsV[1]
                    continue
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] > solutionsV[1] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                
                if sol.y[0][-1] > 40.0*M:
                    break
            i = i + 1
    elif N == 1:
        while True:
            if r[i] < solutionsV[0] and flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break
            elif r[i] < solutionsV[0] and flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if np.isnan(sol.y[0][-1]):
                    flag = 0
                    r[i+1] = solutionsV[0]
                    continue
                if sol.y[0][-1] < 2.0*M:
                    break
            i = i + 1
    elif N == 0:
        while True:
            if flag == 0:
                sol = solve_ivp(minustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] < 2.0*M:
                    break        
            elif flag == 1:
                sol = solve_ivp(plustrajectory,(phi,phi+step), r[i], method='LSODA')
                phi= phi + step
                r[i+1] = sol.y[0][-1]
                if sol.y[0][-1] > 40.0*M:
                    break
            i = i + 1     
    else:
        print("More solutions than expected, check input values.")
    
    return r


solutionsV = solutions()
N = len(solutionsV)
flag = 1
phi0 = 0
step = 0.01
total_angle = 6*np.pi

r0 = solutionsV[2]-0.1*M
print(r0)

r = ivp(r0,phi0,step,N,flag,total_angle)

r = np.trim_zeros(r)
ax = plt.subplot(111, projection='polar')
#ax = plt.subplot(111)
ax.fill(np.linspace(0,2.0*np.pi,100),2*M*np.ones(100),"black",fill=True) 
ax.plot(np.linspace(0,total_angle,len(r)),r, label = "Bound State, E<0")
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.grid(False)
if N==0 and flag == 1:
    ax.set_ylim(0*M,40*M)
elif N==0 and flag == 0:
    ax.set_ylim(0*M,r[0])
elif r[0] < solutionsV[0]:
    ax.set_ylim(0*M,int(solutionsV[0]))
elif r[0] > solutionsV[1]:
    ax.set_ylim(0,40*M)   
ax.set_theta_zero_location('N')
ax.legend()
plt.show()


